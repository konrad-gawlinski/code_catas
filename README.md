Code Catas

\- stairs_left  
```
*  
* *  
* * *  
* * * *  
```
\- stairs_right 
```
      *  
    * *  
  * * *  
* * * *  
```
\- inverted_pyramid 
```
* * * * * * *  
  * * * * *  
    * * *  
      *  
```
\- pyramid 
```
      *  
    * * *  
  * * * * *  
* * * * * * *  
```

\- binary gap  
count the size of biggest '0' gap in a binary number representation  
e.g 00001**0000**101001 the biggest gap size is 4