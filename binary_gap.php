<?php

function measure_biggest_gap($number):int {
    $gapSize = 0;
    $biggestGapSize = 0;
    $n = $number;
    
    while($n > 0) {
        if ($n % 2 == 0) ++$gapSize;
        else {
            if ($gapSize > $biggestGapSize) $biggestGapSize = $gapSize;
            $gapSize = 0;
        }
        $n /= 2;
    }
    
    return $biggestGapSize;
}

echo '9 = ', decbin(9), ', gapsize = ', measure_biggest_gap(9), "\n";
echo '37 = ', decbin(37), ', gapsize = ', measure_biggest_gap(37), "\n";
echo '15 = ', decbin(15), ', gapsize = ', measure_biggest_gap(15), "\n";
echo '529 = ', decbin(529), ', gapsize = ', measure_biggest_gap(529), "\n";
echo '348957834 = ', decbin(348957834), ', gapsize = ', measure_biggest_gap(348957834), "\n";
echo '9483178431431 = ', decbin(9483178431431), ', gapsize = ', measure_biggest_gap(9483178431431), "\n";