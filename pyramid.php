<?php
function draw_pyramid($height) {
    for ($row = 1; $row <= $height; ++$row) {
        for ($i=$height - $row; $i > 0; --$i) {
            echo '  ';
        }
        
        for ($i = $row * 2 - 1; $i > 0; --$i) {
            echo '* ';
        }
        
        echo "\n";
    }
}

draw_pyramid(5);