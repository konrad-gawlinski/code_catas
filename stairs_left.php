<?php

function draw_stairs($stairs) {
    for ($row = 1; $row <= $stairs; ++$row) {
        for($step = 1; $step <= $row; ++$step) {
            echo '* ';
        }   
        echo "\n";
    }
}

draw_stairs(5);