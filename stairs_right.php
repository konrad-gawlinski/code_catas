<?php

function draw_stairs($stairs) {
    for ($row = $stairs; $row > 0; --$row) {
        for($step = $row-1; $step > 0; --$step) {
            echo '  ';
        }
        for($step = $stairs-$row; $step >= 0; --$step) {
            echo '* ';   
        }

        echo "\n";
    }
}

draw_stairs(5);